# cmake_tasks

This repo include some basic prctice questions and Tasks related to CMake. First you have to clone the Repository using commands...

- `git clone (HTTPS link)`
- `cd cmake_tasks`

## How to Run `task_1`

First practice question is simple Hello World program which is executed using the cmake. To view the output of this tasks follow these steps.

---

- cd task_1
- mkdir build
- cd build
- cmake ..
- make

---

The main executable is named `./hello`.

## How to Run `task_2`

This is simple calculator (add, subtract, divide and mltiple) any given numbers. A Static libraray of calculator is created and linked with main application. To view the output of this tasks follow these steps.

### Build in Default mode

---

- cd task_2
- mkdir build
- cd build
- cmake ..
- make

---

### Set Mode using CMakeLists.txt file

- To set build mode in CMakeLists.txt add the following line to your cmakefile `set(CMAKE_BUILD_TYPE Debug)`

### Set Mode using command line

| Compile in Release mode                                | Compile in Debug mode                                |
| ------------------------------------------------------ | ---------------------------------------------------- |
| `cd build`                                             | `cd build`                                           |
| `cmake -DCMAKE_BUILD_TYPE=Release .. (path/to/source)` | `cmake -DCMAKE_BUILD_TYPE=Debug .. (path/to/source)` |
| `make`                                                 | `make`                                               |

The main executable is named `./output`.
